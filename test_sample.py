def func(x):
    return x + 1
    
def len_str(x):
    return len(x) - 1


def test_1():
    assert func(4) == 5
    
def test_2():
    assert len_str('string') == 5