import sys
import os

from fastapi import FastAPI
from fastapi.responses import JSONResponse
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from slsolve import *
#from src.utils.utils import print_result, generate_test_data


app = FastAPI()
@app.get("/")
async def test_data():   
    response = {
        "horizontal" : 10,
        "vertical" : 10,
        "rows" : [  ' ', '2', '0', '2', ' ', ' ', '2', '2', '3', ' ',
                    ' ', ' ', '2', ' ', '1', ' ', ' ', '2', ' ', '3',
                    ' ', '3', ' ', '3', ' ', ' ', ' ', '1', ' ', '2',
                    '2', '2', ' ', ' ', ' ', ' ', ' ', '3', ' ', ' ',
                    '1', ' ', ' ', ' ', ' ', ' ', '2', ' ', ' ', ' ',
                    ' ', ' ', ' ', '2', ' ', ' ', ' ', ' ', ' ', '3',
                    ' ', ' ', '3', ' ', ' ', ' ', ' ', ' ', '2', '2',
                    '2', ' ', '0', ' ', ' ', ' ', '1', ' ', '3', ' ',
                    '2', ' ', '2', ' ', ' ', '2', ' ', '3', ' ', ' ',
                    ' ', '1', '0', '1', ' ', ' ', '1', '1', '2', ' '
                 ],
    }
    return JSONResponse(response)

@app.post("/", response_model=bool)
async def solve(horizontal: int, vertical: int, rows: list[str]):
    puzzle = Puzzle(horizontal, vertical, rows)
    return { 'result': puzzle.can_solve()}
    
